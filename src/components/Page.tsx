import styled from 'styled-components'
import Signal from '../assets/images/Signal.svg'
import Canvas from './Canvas'
import Header from './Header'


const Wrapper = styled.div`
  position: relative;
  display: flex;
  min-width: 30rem;
  max-width: 60rem;
  height: 100%;
  margin: 0 auto;
  flex-direction: column;
  flex-wrap: nowrap;
  align-items: stretch;
  justify-content: flex-start;
  align-content: flex-start;
  z-index: 1;
`

const CanvasWrapper = styled.div`
  position: absolute;
  height: 100%;
  z-index: -1;

  &::after {
    display: block;
    position: absolute;
    content: '';
    top: 0;
    background-image: url("${Signal}");
    width: 100%;
    height: 100%;
  }
`

const Content = styled.div`
  flex: auto;
  margin-top: 5rem;
`

const Page = (props: { children: any }) => {
  return (
    <>
      <CanvasWrapper>
        <Canvas />
      </CanvasWrapper>
      <Header />
      <Wrapper>
        <Content>
          {props.children}
        </Content>
      </Wrapper>
    </>
  )
}

export default Page
