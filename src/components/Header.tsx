import styled from 'styled-components'
import { Link } from 'react-router-dom'

const Wrapper = styled.header`
  position: relative;
  display: flex;
  backdrop-filter: blur(.5rem);
  width: 100%;
  padding: 2rem;
  color: ${props => props.theme.textMain};
  font-size: 2rem;
  user-select: none;
`

const Title = styled.div`
  min-width: 20rem;
  padding-bottom: 1rem;
  width: 100%;
`

const StyledLink = styled(Link)`
  width: 100%;
  text-decoration: none;
  color: inherit;
`


const Header = () => {
  return (
    <Wrapper>
      <StyledLink to="/">
        <Title>
          &#10095; maximch.dev
        </Title>
      </StyledLink>
    </Wrapper>
  )
}

export default Header
