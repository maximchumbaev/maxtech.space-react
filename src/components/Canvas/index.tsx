import styled from 'styled-components'
import { useCanvas, useWindowResize } from './hooks'

const StyledCanvas = styled.canvas`
  display: block;
  width: 100%;
  height: 100%;
`

const Canvas = () => {
  const { width, height } = useWindowResize()
  const { ref } = useCanvas(width, height)

  return (
    <StyledCanvas ref={ref} width={width} height={height} />
  )
}


export default Canvas
