import { useLayoutEffect, useState } from 'react'

type Size = {
  width: number,
  height: number,
}

const useWindowResize = (): Size => {
  const [size, setSize] = useState({ width: 0, height: 0 })

  useLayoutEffect(() => {
    const updateSize = () => {
      setSize({
        width: document.body.clientWidth,
        height: document.body.clientHeight,
      })
    }

    window.addEventListener('resize', updateSize)
    updateSize()
    return () => window.removeEventListener('resize', updateSize)
  }, [])

  return size
}

export default useWindowResize
