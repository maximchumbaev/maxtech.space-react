import useCanvas from './UseCanvas'
import useWindowResize from './UseWindowResize'

export {
  useCanvas,
  useWindowResize,
}
