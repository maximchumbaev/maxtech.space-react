import {
  MutableRefObject,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from 'react'

interface UseCanvasReturn {
  ref: MutableRefObject<HTMLCanvasElement | null>
}

const draw = (
  ctx: CanvasRenderingContext2D,
  width: number,
  height: number,
  x: number,
  y: number,
): void => {
  const size = 200
  ctx.beginPath()
  ctx.save()
  ctx.clearRect(0, 0, width, height)
  ctx.fillStyle = '#1E512866'
  ctx.filter = "blur(50px)"
  ctx.arc(x, y, size / 2, 0, Math.PI * 2)
  ctx.fill()
  ctx.restore()
  ctx.closePath()
  // fill(ctx, width, height)
}

const useCanvas = (width: number, height: number): UseCanvasReturn => {
  const [context, setContext] = useState<CanvasRenderingContext2D | null>(null)
  const ref = useRef(null)

  useEffect(() => {
    if (!context && ref) {
      const ctx = getContext()
      if (ctx) {
        setContext(ctx)
      }
    } else if (context) {
      draw(context, width, height, -1000, -1000)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context, width, height])

  useLayoutEffect(() => {
    window.addEventListener('mousemove', (e: MouseEvent) => {
      if (context) {
        draw(
          context,
          width,
          height,
          e.clientX + window.scrollX,
          e.clientY + window.scrollY,
        )
      }
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context, width, height])

  const getContext = (): CanvasRenderingContext2D | null => {
    const canvas = ref.current as HTMLCanvasElement | null
    if (canvas) {
      return canvas.getContext('2d') as CanvasRenderingContext2D
    }
    return null
  }

  return { ref }
}

export default useCanvas
