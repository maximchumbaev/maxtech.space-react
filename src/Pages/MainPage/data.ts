interface OpenProjectI {
  name: string
  link?: string
  sources?: string
  skills?: string[]
}

interface LinkI {
  name: string
  link: string
}

const openProjects: OpenProjectI[] = [
  {
    name: 'webhook server',
    link: 'https://webhook.maximch.dev',
    sources: 'https://gitlab.com/maximchumbaev/webhooker',
    skills: ['Python', 'Django', 'PostgreSQL', 'Docker'],
  },
  {
    name: 'webhook client',
    sources: 'https://gitlab.com/maximchumbaev/webhooker-client',
    skills: ['Python', 'Docker'],
  },
  {
    name: 'this site',
    link: '/',
    sources: 'https://gitlab.com/maximchumbaev/maximch.dev-react',
    skills: ['React', 'TypeScript'],
  },
  {
    name: 'coming soon...',
  },
]

const links: LinkI[] = [
  {
    name: 'GitLab',
    link: 'https://gitlab.com/maximchumbaev',
  },
]

export {
  links,
  openProjects,
}
