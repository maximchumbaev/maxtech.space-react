import styled from 'styled-components'
import Typewriter from 'typewriter-effect'
import { links, openProjects } from './data'
import { writeGreeting } from './helpers'


const Wrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  padding: 1rem;

  & > * {
    font-size: 4rem;
    color: ${props => props.theme.textSecondary};
  }
`

const Greeting = styled.div`
  height: 13rem;
`

const Link = styled.a`
  width: max-content;
  text-decoration: none;
  color: inherit;

  &:hover {
    color: ${props => props.theme.textMain}
  }
`

const Heading = styled.p`
  margin: 2rem 0;
  font-size: 3rem;
  font-weight: bold;
  color: ${props => props.theme.textMain}
`

const List = styled.ul`
  display: flex;
  flex-direction: column;
  gap: 1.3rem;
  margin-bottom: 4rem;
  list-style: none;
  font-size: 2rem;
`

const ListItem = styled.li`
  display: flex;
  align-items: baseline;
  margin-left: 2rem;
  white-space: pre-wrap;

  &::before {
    content: '';
    width: 1rem;
    height: 1rem;
    margin-right: 1rem;
    border-radius: 1rem;
    background-color: ${props => props.theme.textMain};
  }
`

const ListItemContent = styled.div`
  display: flex;
  flex-direction: column;
`

const Skills = styled.ul`
  margin-left: 5rem;
`

const MainPage = () => {
  return (
    <Wrapper>
      <Greeting>
        <Typewriter onInit={writeGreeting} />
      </Greeting>
      <Heading>My projects:</Heading>
      <List>
        {openProjects.map(({ name, link, sources, skills }, i) => (
          <ListItem key={i}>
            <ListItemContent>
              <p>
                {name}
                {(link || sources) && ' ('}
                {link && <Link href={link}>link</Link>}
                {link && ', '}
                {sources && <Link href={sources} target="_blank">
                    sources
                </Link>}
                {(link || sources) && ')'}
              </p>
              {
                skills &&
                  <Skills>
                    {skills.map((skill, j) => (
                      <li key={j}>{skill}</li>
                    ))}
                  </Skills>
              }
            </ListItemContent>
          </ListItem>
        ))}
      </List>
      {
        links.length > 0 &&
        <>
          <Heading>Links:</Heading>
          <List>
            {links.map(({ name, link }, i) => (
              <ListItem key={i}>
                <Link href={link}>{name}</Link>
              </ListItem>
            ))}
          </List>
        </>
      }
    </Wrapper>
  )
}


export default MainPage
