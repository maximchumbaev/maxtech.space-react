import { TypewriterClass } from 'typewriter-effect'


const writeGreeting = (typewriter: TypewriterClass) => {
  typewriter
    .typeString('Hi')
    .pauseFor(400)
    .typeString(', I\'m Max')
    .pauseFor(400)
    .typeString(', backend')
    .pauseFor(300)
    .deleteChars(7)
    .pauseFor(300)
    .typeString('frontend')
    .pauseFor(300)
    .deleteChars(8)
    .pauseFor(300)
    .typeString('full-stack developer')
    .start()
}


export { writeGreeting, }
