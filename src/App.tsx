import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import Page from './components/Page'
import MainPage from './Pages/MainPage'

const theme = {
  main: '#1E5128',
  background: '#191A19',
  textMain: '#D8E9A8',
  textSecondary: '#4E9F3D',
}

function App() {
  return (
    <Router>
      <ThemeProvider theme={theme}>
        <Page>
          <Routes>
            <Route element={<MainPage />} path="/" />
          </Routes>
        </Page>
      </ThemeProvider>
    </Router>
  )
}

export default App
